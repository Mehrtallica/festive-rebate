﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using EntitySpaces.Core;
using EntitySpaces.DynamicQuery;

namespace FestiveRabateExport.Code
{
    static class Shared
    {
        public static DataTable ExecuteManualQuery(string sql)
        {
            esUtility util = new esUtility();
            return util.FillDataTable(esQueryType.Text, sql);
        }

        public static bool ExecuteNonQuery(string sql)
        {
            try
            {
                esUtility util = new esUtility();
                util.ExecuteNonQuery(esQueryType.Text, sql);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

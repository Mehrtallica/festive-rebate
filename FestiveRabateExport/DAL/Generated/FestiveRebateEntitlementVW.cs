
/*
===============================================================================
                    EntitySpaces Studio by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 6/15/2017 11:33:40 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace DAL
{
	/// <summary>
	/// Encapsulates the 'FestiveRebateEntitlement_VW' view
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[KnownType(typeof(FestiveRebateEntitlementVW))]	
	[XmlType("FestiveRebateEntitlementVW")]
	public partial class FestiveRebateEntitlementVW : esFestiveRebateEntitlementVW
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new FestiveRebateEntitlementVW();
		}
		
		#region Static Quick Access Methods
		
		#endregion

		
		
		override protected string GetConnectionName()
		{
			return "root";
		}			
		
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[Serializable]
	[CollectionDataContract]
	[XmlType("FestiveRebateEntitlementVWCollection")]
	public partial class FestiveRebateEntitlementVWCollection : esFestiveRebateEntitlementVWCollection, IEnumerable<FestiveRebateEntitlementVW>
	{

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(FestiveRebateEntitlementVW))]
		public class FestiveRebateEntitlementVWCollectionWCFPacket : esCollectionWCFPacket<FestiveRebateEntitlementVWCollection>
		{
			public static implicit operator FestiveRebateEntitlementVWCollection(FestiveRebateEntitlementVWCollectionWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator FestiveRebateEntitlementVWCollectionWCFPacket(FestiveRebateEntitlementVWCollection collection)
			{
				return new FestiveRebateEntitlementVWCollectionWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "root";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class FestiveRebateEntitlementVWQuery : esFestiveRebateEntitlementVWQuery
	{
		public FestiveRebateEntitlementVWQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	

		override protected string GetQueryName()
		{
			return "FestiveRebateEntitlementVWQuery";
		}
		
		
		override protected string GetConnectionName()
		{
			return "root";
		}			
	
		#region Explicit Casts
		
		public static explicit operator string(FestiveRebateEntitlementVWQuery query)
		{
			return FestiveRebateEntitlementVWQuery.SerializeHelper.ToXml(query);
		}

		public static explicit operator FestiveRebateEntitlementVWQuery(string query)
		{
			return (FestiveRebateEntitlementVWQuery)FestiveRebateEntitlementVWQuery.SerializeHelper.FromXml(query, typeof(FestiveRebateEntitlementVWQuery));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esFestiveRebateEntitlementVW : esEntity
	{
		public esFestiveRebateEntitlementVW()
		{

		}
		
		#region LoadByPrimaryKey
		
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.MFGNO
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Mfgno
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.Mfgno);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.Mfgno, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.Mfgno);
				}
			}
		}		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.EntitleDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EntitleDate
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.EntitleDate);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.EntitleDate, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.EntitleDate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.TrxVolume
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TrxVolume
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.TrxVolume);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.TrxVolume, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.TrxVolume);
				}
			}
		}		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.TrxValue
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TrxValue
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.TrxValue);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.TrxValue, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.TrxValue);
				}
			}
		}		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.RebateAmount
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String RebateAmount
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.RebateAmount);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.RebateAmount, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.RebateAmount);
				}
			}
		}		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.EffectiveDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String EffectiveDate
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.EffectiveDate);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.EffectiveDate, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.EffectiveDate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.ExpiryDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String ExpiryDate
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.ExpiryDate);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.ExpiryDate, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.ExpiryDate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.RebateID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String RebateID
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.RebateID);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.RebateID, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.RebateID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to FestiveRebateEntitlement_VW.VoucherNo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String VoucherNo
		{
			get
			{
				return base.GetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.VoucherNo);
			}
			
			set
			{
				if(base.SetSystemString(FestiveRebateEntitlementVWMetadata.ColumnNames.VoucherNo, value))
				{
					OnPropertyChanged(FestiveRebateEntitlementVWMetadata.PropertyNames.VoucherNo);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Mfgno": this.str().Mfgno = (string)value; break;							
						case "EntitleDate": this.str().EntitleDate = (string)value; break;							
						case "TrxVolume": this.str().TrxVolume = (string)value; break;							
						case "TrxValue": this.str().TrxValue = (string)value; break;							
						case "RebateAmount": this.str().RebateAmount = (string)value; break;							
						case "EffectiveDate": this.str().EffectiveDate = (string)value; break;							
						case "ExpiryDate": this.str().ExpiryDate = (string)value; break;							
						case "RebateID": this.str().RebateID = (string)value; break;							
						case "VoucherNo": this.str().VoucherNo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esFestiveRebateEntitlementVW entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Mfgno
			{
				get
				{
					System.String data = entity.Mfgno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mfgno = null;
					else entity.Mfgno = Convert.ToString(value);
				}
			}
				
			public System.String EntitleDate
			{
				get
				{
					System.String data = entity.EntitleDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EntitleDate = null;
					else entity.EntitleDate = Convert.ToString(value);
				}
			}
				
			public System.String TrxVolume
			{
				get
				{
					System.String data = entity.TrxVolume;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxVolume = null;
					else entity.TrxVolume = Convert.ToString(value);
				}
			}
				
			public System.String TrxValue
			{
				get
				{
					System.String data = entity.TrxValue;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxValue = null;
					else entity.TrxValue = Convert.ToString(value);
				}
			}
				
			public System.String RebateAmount
			{
				get
				{
					System.String data = entity.RebateAmount;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebateAmount = null;
					else entity.RebateAmount = Convert.ToString(value);
				}
			}
				
			public System.String EffectiveDate
			{
				get
				{
					System.String data = entity.EffectiveDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EffectiveDate = null;
					else entity.EffectiveDate = Convert.ToString(value);
				}
			}
				
			public System.String ExpiryDate
			{
				get
				{
					System.String data = entity.ExpiryDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExpiryDate = null;
					else entity.ExpiryDate = Convert.ToString(value);
				}
			}
				
			public System.String RebateID
			{
				get
				{
					System.String data = entity.RebateID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebateID = null;
					else entity.RebateID = Convert.ToString(value);
				}
			}
				
			public System.String VoucherNo
			{
				get
				{
					System.String data = entity.VoucherNo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VoucherNo = null;
					else entity.VoucherNo = Convert.ToString(value);
				}
			}
			

			private esFestiveRebateEntitlementVW entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return FestiveRebateEntitlementVWMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public FestiveRebateEntitlementVWQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FestiveRebateEntitlementVWQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(FestiveRebateEntitlementVWQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(FestiveRebateEntitlementVWQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}

		#endregion
		
        [IgnoreDataMember]
		private FestiveRebateEntitlementVWQuery query;		
	}



	[Serializable]
	abstract public partial class esFestiveRebateEntitlementVWCollection : esEntityCollection<FestiveRebateEntitlementVW>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return FestiveRebateEntitlementVWMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "FestiveRebateEntitlementVWCollection";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public FestiveRebateEntitlementVWQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FestiveRebateEntitlementVWQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(FestiveRebateEntitlementVWQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FestiveRebateEntitlementVWQuery();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(FestiveRebateEntitlementVWQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((FestiveRebateEntitlementVWQuery)query);
		}

		#endregion
		
		private FestiveRebateEntitlementVWQuery query;
	}



	[Serializable]
	abstract public partial class esFestiveRebateEntitlementVWQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return FestiveRebateEntitlementVWMetadata.Meta();
			}
		}	
		
		#region QueryItemFromName
		
        protected override esQueryItem QueryItemFromName(string name)
        {
            switch (name)
            {
				case "Mfgno": return this.Mfgno;
				case "EntitleDate": return this.EntitleDate;
				case "TrxVolume": return this.TrxVolume;
				case "TrxValue": return this.TrxValue;
				case "RebateAmount": return this.RebateAmount;
				case "EffectiveDate": return this.EffectiveDate;
				case "ExpiryDate": return this.ExpiryDate;
				case "RebateID": return this.RebateID;
				case "VoucherNo": return this.VoucherNo;

                default: return null;
            }
        }		
		
		#endregion
		
		#region esQueryItems

		public esQueryItem Mfgno
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.Mfgno, esSystemType.String); }
		} 
		
		public esQueryItem EntitleDate
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.EntitleDate, esSystemType.String); }
		} 
		
		public esQueryItem TrxVolume
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.TrxVolume, esSystemType.String); }
		} 
		
		public esQueryItem TrxValue
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.TrxValue, esSystemType.String); }
		} 
		
		public esQueryItem RebateAmount
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.RebateAmount, esSystemType.String); }
		} 
		
		public esQueryItem EffectiveDate
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.EffectiveDate, esSystemType.String); }
		} 
		
		public esQueryItem ExpiryDate
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.ExpiryDate, esSystemType.String); }
		} 
		
		public esQueryItem RebateID
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.RebateID, esSystemType.String); }
		} 
		
		public esQueryItem VoucherNo
		{
			get { return new esQueryItem(this, FestiveRebateEntitlementVWMetadata.ColumnNames.VoucherNo, esSystemType.String); }
		} 
		
		#endregion
		
	}



	[Serializable]
	public partial class FestiveRebateEntitlementVWMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected FestiveRebateEntitlementVWMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.Mfgno, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.Mfgno;
			c.CharacterMaxLength = 10;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.EntitleDate, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.EntitleDate;
			c.CharacterMaxLength = 8;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.TrxVolume, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.TrxVolume;
			c.CharacterMaxLength = 4;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.TrxValue, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.TrxValue;
			c.CharacterMaxLength = 6;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.RebateAmount, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.RebateAmount;
			c.CharacterMaxLength = 6;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.EffectiveDate, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.EffectiveDate;
			c.CharacterMaxLength = 8;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.ExpiryDate, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.ExpiryDate;
			c.CharacterMaxLength = 8;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.RebateID, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.RebateID;
			c.CharacterMaxLength = 4;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(FestiveRebateEntitlementVWMetadata.ColumnNames.VoucherNo, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = FestiveRebateEntitlementVWMetadata.PropertyNames.VoucherNo;
			c.CharacterMaxLength = 20;
			c.IsNullable = true;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public FestiveRebateEntitlementVWMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Mfgno = "MFGNO";
			 public const string EntitleDate = "EntitleDate";
			 public const string TrxVolume = "TrxVolume";
			 public const string TrxValue = "TrxValue";
			 public const string RebateAmount = "RebateAmount";
			 public const string EffectiveDate = "EffectiveDate";
			 public const string ExpiryDate = "ExpiryDate";
			 public const string RebateID = "RebateID";
			 public const string VoucherNo = "VoucherNo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Mfgno = "Mfgno";
			 public const string EntitleDate = "EntitleDate";
			 public const string TrxVolume = "TrxVolume";
			 public const string TrxValue = "TrxValue";
			 public const string RebateAmount = "RebateAmount";
			 public const string EffectiveDate = "EffectiveDate";
			 public const string ExpiryDate = "ExpiryDate";
			 public const string RebateID = "RebateID";
			 public const string VoucherNo = "VoucherNo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(FestiveRebateEntitlementVWMetadata))
			{
				if(FestiveRebateEntitlementVWMetadata.mapDelegates == null)
				{
					FestiveRebateEntitlementVWMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (FestiveRebateEntitlementVWMetadata.meta == null)
				{
					FestiveRebateEntitlementVWMetadata.meta = new FestiveRebateEntitlementVWMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Mfgno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EntitleDate", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TrxVolume", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TrxValue", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("RebateAmount", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EffectiveDate", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ExpiryDate", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("RebateID", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VoucherNo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "FestiveRebateEntitlement_VW";
				meta.Destination = "FestiveRebateEntitlement_VW";
				
				meta.spInsert = "proc_FestiveRebateEntitlement_VWInsert";				
				meta.spUpdate = "proc_FestiveRebateEntitlement_VWUpdate";		
				meta.spDelete = "proc_FestiveRebateEntitlement_VWDelete";
				meta.spLoadAll = "proc_FestiveRebateEntitlement_VWLoadAll";
				meta.spLoadByPrimaryKey = "proc_FestiveRebateEntitlement_VWLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private FestiveRebateEntitlementVWMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}

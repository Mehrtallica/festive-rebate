
/*
===============================================================================
                    EntitySpaces Studio by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 6/15/2017 11:33:31 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace DAL
{
	/// <summary>
	/// Encapsulates the 'tblFestiveRebateTrx' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[KnownType(typeof(TblFestiveRebateTrx))]	
	[XmlType("TblFestiveRebateTrx")]
	public partial class TblFestiveRebateTrx : esTblFestiveRebateTrx
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TblFestiveRebateTrx();
		}
		
		#region Static Quick Access Methods
		
		#endregion

		
		
		override protected string GetConnectionName()
		{
			return "root";
		}			
		
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TblFestiveRebateTrxCollection")]
	public partial class TblFestiveRebateTrxCollection : esTblFestiveRebateTrxCollection, IEnumerable<TblFestiveRebateTrx>
	{

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TblFestiveRebateTrx))]
		public class TblFestiveRebateTrxCollectionWCFPacket : esCollectionWCFPacket<TblFestiveRebateTrxCollection>
		{
			public static implicit operator TblFestiveRebateTrxCollection(TblFestiveRebateTrxCollectionWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TblFestiveRebateTrxCollectionWCFPacket(TblFestiveRebateTrxCollection collection)
			{
				return new TblFestiveRebateTrxCollectionWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "root";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TblFestiveRebateTrxQuery : esTblFestiveRebateTrxQuery
	{
		public TblFestiveRebateTrxQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	

		override protected string GetQueryName()
		{
			return "TblFestiveRebateTrxQuery";
		}
		
		
		override protected string GetConnectionName()
		{
			return "root";
		}			
	
		#region Explicit Casts
		
		public static explicit operator string(TblFestiveRebateTrxQuery query)
		{
			return TblFestiveRebateTrxQuery.SerializeHelper.ToXml(query);
		}

		public static explicit operator TblFestiveRebateTrxQuery(string query)
		{
			return (TblFestiveRebateTrxQuery)TblFestiveRebateTrxQuery.SerializeHelper.FromXml(query, typeof(TblFestiveRebateTrxQuery));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTblFestiveRebateTrx : esEntity
	{
		public esTblFestiveRebateTrx()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey()
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic();
			else
				return LoadByPrimaryKeyStoredProcedure();
		}

        //public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, )
        //{
        //    if (sqlAccessType == esSqlAccessType.DynamicSQL)
        //        return LoadByPrimaryKeyDynamic();
        //    else
        //        return LoadByPrimaryKeyStoredProcedure();
        //}

		private bool LoadByPrimaryKeyDynamic()
		{
			TblFestiveRebateTrxQuery query = new TblFestiveRebateTrxQuery();
			query.Where();
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure()
		{
			esParameters parms = new esParameters();

			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.TrxDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? TrxDate
		{
			get
			{
				return base.GetSystemDateTime(TblFestiveRebateTrxMetadata.ColumnNames.TrxDate);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblFestiveRebateTrxMetadata.ColumnNames.TrxDate, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxDate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.TrxType
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Char? TrxType
		{
			get
			{
				return base.GetSystemChar(TblFestiveRebateTrxMetadata.ColumnNames.TrxType);
			}
			
			set
			{
				if(base.SetSystemChar(TblFestiveRebateTrxMetadata.ColumnNames.TrxType, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxType);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.TrxCode
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TrxCode
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.TrxCode);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.TrxCode, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxCode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.MCCCode
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MCCCode
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.MCCCode);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.MCCCode, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.MCCCode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.MerchantID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MerchantID
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.MerchantID);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.MerchantID, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.MerchantID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.MerchantName
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String MerchantName
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.MerchantName);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.MerchantName, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.MerchantName);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.TrxCurrCode
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TrxCurrCode
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.TrxCurrCode);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.TrxCurrCode, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxCurrCode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.BillCurrCode
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String BillCurrCode
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.BillCurrCode);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.BillCurrCode, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.BillCurrCode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.TrxAmt
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? TrxAmt
		{
			get
			{
				return base.GetSystemDecimal(TblFestiveRebateTrxMetadata.ColumnNames.TrxAmt);
			}
			
			set
			{
				if(base.SetSystemDecimal(TblFestiveRebateTrxMetadata.ColumnNames.TrxAmt, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxAmt);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.CountryCode
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String CountryCode
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.CountryCode);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.CountryCode, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.CountryCode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.POSMode
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String POSMode
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.POSMode);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.POSMode, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.POSMode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.CardNo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String CardNo
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.CardNo);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.CardNo, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.CardNo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.BillAmt
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? BillAmt
		{
			get
			{
				return base.GetSystemDecimal(TblFestiveRebateTrxMetadata.ColumnNames.BillAmt);
			}
			
			set
			{
				if(base.SetSystemDecimal(TblFestiveRebateTrxMetadata.ColumnNames.BillAmt, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.BillAmt);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.RefNo
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String RefNo
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.RefNo);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.RefNo, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.RefNo);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.AuthCode
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String AuthCode
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.AuthCode);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.AuthCode, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.AuthCode);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.Remarks
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String Remarks
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.Remarks);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.Remarks, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.Remarks);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.TrxParamXML
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String TrxParamXML
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.TrxParamXML);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.TrxParamXML, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxParamXML);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.UpdXMLStatus
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Boolean? UpdXMLStatus
		{
			get
			{
				return base.GetSystemBoolean(TblFestiveRebateTrxMetadata.ColumnNames.UpdXMLStatus);
			}
			
			set
			{
				if(base.SetSystemBoolean(TblFestiveRebateTrxMetadata.ColumnNames.UpdXMLStatus, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.UpdXMLStatus);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.BatchID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String BatchID
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.BatchID);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.BatchID, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.BatchID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.CardNoShow
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.String CardNoShow
		{
			get
			{
				return base.GetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.CardNoShow);
			}
			
			set
			{
				if(base.SetSystemString(TblFestiveRebateTrxMetadata.ColumnNames.CardNoShow, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.CardNoShow);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.MemberID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Guid? MemberID
		{
			get
			{
				return base.GetSystemGuid(TblFestiveRebateTrxMetadata.ColumnNames.MemberID);
			}
			
			set
			{
				if(base.SetSystemGuid(TblFestiveRebateTrxMetadata.ColumnNames.MemberID, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.MemberID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.MemberTypeID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Guid? MemberTypeID
		{
			get
			{
				return base.GetSystemGuid(TblFestiveRebateTrxMetadata.ColumnNames.MemberTypeID);
			}
			
			set
			{
				if(base.SetSystemGuid(TblFestiveRebateTrxMetadata.ColumnNames.MemberTypeID, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.MemberTypeID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.ProductVariantID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Guid? ProductVariantID
		{
			get
			{
				return base.GetSystemGuid(TblFestiveRebateTrxMetadata.ColumnNames.ProductVariantID);
			}
			
			set
			{
				if(base.SetSystemGuid(TblFestiveRebateTrxMetadata.ColumnNames.ProductVariantID, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.ProductVariantID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.CardExpiryDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? CardExpiryDate
		{
			get
			{
				return base.GetSystemDateTime(TblFestiveRebateTrxMetadata.ColumnNames.CardExpiryDate);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblFestiveRebateTrxMetadata.ColumnNames.CardExpiryDate, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.CardExpiryDate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.CalcProcessStatus
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Char? CalcProcessStatus
		{
			get
			{
				return base.GetSystemChar(TblFestiveRebateTrxMetadata.ColumnNames.CalcProcessStatus);
			}
			
			set
			{
				if(base.SetSystemChar(TblFestiveRebateTrxMetadata.ColumnNames.CalcProcessStatus, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.CalcProcessStatus);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.TrxID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int64? TrxID
		{
			get
			{
				return base.GetSystemInt64(TblFestiveRebateTrxMetadata.ColumnNames.TrxID);
			}
			
			set
			{
				if(base.SetSystemInt64(TblFestiveRebateTrxMetadata.ColumnNames.TrxID, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.UpdLoyaltyStatus
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Char? UpdLoyaltyStatus
		{
			get
			{
				return base.GetSystemChar(TblFestiveRebateTrxMetadata.ColumnNames.UpdLoyaltyStatus);
			}
			
			set
			{
				if(base.SetSystemChar(TblFestiveRebateTrxMetadata.ColumnNames.UpdLoyaltyStatus, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.UpdLoyaltyStatus);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.DateTimeLog
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? DateTimeLog
		{
			get
			{
				return base.GetSystemDateTime(TblFestiveRebateTrxMetadata.ColumnNames.DateTimeLog);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblFestiveRebateTrxMetadata.ColumnNames.DateTimeLog, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.DateTimeLog);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.TempStatus
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int32? TempStatus
		{
			get
			{
				return base.GetSystemInt32(TblFestiveRebateTrxMetadata.ColumnNames.TempStatus);
			}
			
			set
			{
				if(base.SetSystemInt32(TblFestiveRebateTrxMetadata.ColumnNames.TempStatus, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TempStatus);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.SettlementDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? SettlementDate
		{
			get
			{
				return base.GetSystemDateTime(TblFestiveRebateTrxMetadata.ColumnNames.SettlementDate);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblFestiveRebateTrxMetadata.ColumnNames.SettlementDate, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.SettlementDate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.decRebate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? DecRebate
		{
			get
			{
				return base.GetSystemDecimal(TblFestiveRebateTrxMetadata.ColumnNames.DecRebate);
			}
			
			set
			{
				if(base.SetSystemDecimal(TblFestiveRebateTrxMetadata.ColumnNames.DecRebate, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.DecRebate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestiveRebateTrx.idRebate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int64? IdRebate
		{
			get
			{
				return base.GetSystemInt64(TblFestiveRebateTrxMetadata.ColumnNames.IdRebate);
			}
			
			set
			{
				if(base.SetSystemInt64(TblFestiveRebateTrxMetadata.ColumnNames.IdRebate, value))
				{
					OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.IdRebate);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TrxDate": this.str().TrxDate = (string)value; break;							
						case "TrxType": this.str().TrxType = (string)value; break;							
						case "TrxCode": this.str().TrxCode = (string)value; break;							
						case "MCCCode": this.str().MCCCode = (string)value; break;							
						case "MerchantID": this.str().MerchantID = (string)value; break;							
						case "MerchantName": this.str().MerchantName = (string)value; break;							
						case "TrxCurrCode": this.str().TrxCurrCode = (string)value; break;							
						case "BillCurrCode": this.str().BillCurrCode = (string)value; break;							
						case "TrxAmt": this.str().TrxAmt = (string)value; break;							
						case "CountryCode": this.str().CountryCode = (string)value; break;							
						case "POSMode": this.str().POSMode = (string)value; break;							
						case "CardNo": this.str().CardNo = (string)value; break;							
						case "BillAmt": this.str().BillAmt = (string)value; break;							
						case "RefNo": this.str().RefNo = (string)value; break;							
						case "AuthCode": this.str().AuthCode = (string)value; break;							
						case "Remarks": this.str().Remarks = (string)value; break;							
						case "TrxParamXML": this.str().TrxParamXML = (string)value; break;							
						case "UpdXMLStatus": this.str().UpdXMLStatus = (string)value; break;							
						case "BatchID": this.str().BatchID = (string)value; break;							
						case "CardNoShow": this.str().CardNoShow = (string)value; break;							
						case "MemberID": this.str().MemberID = (string)value; break;							
						case "MemberTypeID": this.str().MemberTypeID = (string)value; break;							
						case "ProductVariantID": this.str().ProductVariantID = (string)value; break;							
						case "CardExpiryDate": this.str().CardExpiryDate = (string)value; break;							
						case "CalcProcessStatus": this.str().CalcProcessStatus = (string)value; break;							
						case "TrxID": this.str().TrxID = (string)value; break;							
						case "UpdLoyaltyStatus": this.str().UpdLoyaltyStatus = (string)value; break;							
						case "DateTimeLog": this.str().DateTimeLog = (string)value; break;							
						case "TempStatus": this.str().TempStatus = (string)value; break;							
						case "SettlementDate": this.str().SettlementDate = (string)value; break;							
						case "DecRebate": this.str().DecRebate = (string)value; break;							
						case "IdRebate": this.str().IdRebate = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TrxDate":
						
							if (value == null || value is System.DateTime)
								this.TrxDate = (System.DateTime?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxDate);
							break;
						
						case "TrxType":
						
							if (value == null || value is System.Char)
								this.TrxType = (System.Char?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxType);
							break;
						
						case "TrxAmt":
						
							if (value == null || value is System.Decimal)
								this.TrxAmt = (System.Decimal?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxAmt);
							break;
						
						case "BillAmt":
						
							if (value == null || value is System.Decimal)
								this.BillAmt = (System.Decimal?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.BillAmt);
							break;
						
						case "UpdXMLStatus":
						
							if (value == null || value is System.Boolean)
								this.UpdXMLStatus = (System.Boolean?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.UpdXMLStatus);
							break;
						
						case "MemberID":
						
							if (value == null || value is System.Guid)
								this.MemberID = (System.Guid?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.MemberID);
							break;
						
						case "MemberTypeID":
						
							if (value == null || value is System.Guid)
								this.MemberTypeID = (System.Guid?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.MemberTypeID);
							break;
						
						case "ProductVariantID":
						
							if (value == null || value is System.Guid)
								this.ProductVariantID = (System.Guid?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.ProductVariantID);
							break;
						
						case "CardExpiryDate":
						
							if (value == null || value is System.DateTime)
								this.CardExpiryDate = (System.DateTime?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.CardExpiryDate);
							break;
						
						case "CalcProcessStatus":
						
							if (value == null || value is System.Char)
								this.CalcProcessStatus = (System.Char?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.CalcProcessStatus);
							break;
						
						case "TrxID":
						
							if (value == null || value is System.Int64)
								this.TrxID = (System.Int64?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TrxID);
							break;
						
						case "UpdLoyaltyStatus":
						
							if (value == null || value is System.Char)
								this.UpdLoyaltyStatus = (System.Char?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.UpdLoyaltyStatus);
							break;
						
						case "DateTimeLog":
						
							if (value == null || value is System.DateTime)
								this.DateTimeLog = (System.DateTime?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.DateTimeLog);
							break;
						
						case "TempStatus":
						
							if (value == null || value is System.Int32)
								this.TempStatus = (System.Int32?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.TempStatus);
							break;
						
						case "SettlementDate":
						
							if (value == null || value is System.DateTime)
								this.SettlementDate = (System.DateTime?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.SettlementDate);
							break;
						
						case "DecRebate":
						
							if (value == null || value is System.Decimal)
								this.DecRebate = (System.Decimal?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.DecRebate);
							break;
						
						case "IdRebate":
						
							if (value == null || value is System.Int64)
								this.IdRebate = (System.Int64?)value;
								OnPropertyChanged(TblFestiveRebateTrxMetadata.PropertyNames.IdRebate);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTblFestiveRebateTrx entity)
			{
				this.entity = entity;
			}
			
	
			public System.String TrxDate
			{
				get
				{
					System.DateTime? data = entity.TrxDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxDate = null;
					else entity.TrxDate = Convert.ToDateTime(value);
				}
			}
				
			public System.String TrxType
			{
				get
				{
					System.Char? data = entity.TrxType;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxType = null;
					else entity.TrxType = Convert.ToChar(value);
				}
			}
				
			public System.String TrxCode
			{
				get
				{
					System.String data = entity.TrxCode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxCode = null;
					else entity.TrxCode = Convert.ToString(value);
				}
			}
				
			public System.String MCCCode
			{
				get
				{
					System.String data = entity.MCCCode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MCCCode = null;
					else entity.MCCCode = Convert.ToString(value);
				}
			}
				
			public System.String MerchantID
			{
				get
				{
					System.String data = entity.MerchantID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MerchantID = null;
					else entity.MerchantID = Convert.ToString(value);
				}
			}
				
			public System.String MerchantName
			{
				get
				{
					System.String data = entity.MerchantName;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MerchantName = null;
					else entity.MerchantName = Convert.ToString(value);
				}
			}
				
			public System.String TrxCurrCode
			{
				get
				{
					System.String data = entity.TrxCurrCode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxCurrCode = null;
					else entity.TrxCurrCode = Convert.ToString(value);
				}
			}
				
			public System.String BillCurrCode
			{
				get
				{
					System.String data = entity.BillCurrCode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BillCurrCode = null;
					else entity.BillCurrCode = Convert.ToString(value);
				}
			}
				
			public System.String TrxAmt
			{
				get
				{
					System.Decimal? data = entity.TrxAmt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxAmt = null;
					else entity.TrxAmt = Convert.ToDecimal(value);
				}
			}
				
			public System.String CountryCode
			{
				get
				{
					System.String data = entity.CountryCode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CountryCode = null;
					else entity.CountryCode = Convert.ToString(value);
				}
			}
				
			public System.String POSMode
			{
				get
				{
					System.String data = entity.POSMode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.POSMode = null;
					else entity.POSMode = Convert.ToString(value);
				}
			}
				
			public System.String CardNo
			{
				get
				{
					System.String data = entity.CardNo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CardNo = null;
					else entity.CardNo = Convert.ToString(value);
				}
			}
				
			public System.String BillAmt
			{
				get
				{
					System.Decimal? data = entity.BillAmt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BillAmt = null;
					else entity.BillAmt = Convert.ToDecimal(value);
				}
			}
				
			public System.String RefNo
			{
				get
				{
					System.String data = entity.RefNo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RefNo = null;
					else entity.RefNo = Convert.ToString(value);
				}
			}
				
			public System.String AuthCode
			{
				get
				{
					System.String data = entity.AuthCode;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AuthCode = null;
					else entity.AuthCode = Convert.ToString(value);
				}
			}
				
			public System.String Remarks
			{
				get
				{
					System.String data = entity.Remarks;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Remarks = null;
					else entity.Remarks = Convert.ToString(value);
				}
			}
				
			public System.String TrxParamXML
			{
				get
				{
					System.String data = entity.TrxParamXML;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxParamXML = null;
					else entity.TrxParamXML = Convert.ToString(value);
				}
			}
				
			public System.String UpdXMLStatus
			{
				get
				{
					System.Boolean? data = entity.UpdXMLStatus;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UpdXMLStatus = null;
					else entity.UpdXMLStatus = Convert.ToBoolean(value);
				}
			}
				
			public System.String BatchID
			{
				get
				{
					System.String data = entity.BatchID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BatchID = null;
					else entity.BatchID = Convert.ToString(value);
				}
			}
				
			public System.String CardNoShow
			{
				get
				{
					System.String data = entity.CardNoShow;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CardNoShow = null;
					else entity.CardNoShow = Convert.ToString(value);
				}
			}
				
			public System.String MemberID
			{
				get
				{
					System.Guid? data = entity.MemberID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MemberID = null;
					else entity.MemberID = new Guid(value);
				}
			}
				
			public System.String MemberTypeID
			{
				get
				{
					System.Guid? data = entity.MemberTypeID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MemberTypeID = null;
					else entity.MemberTypeID = new Guid(value);
				}
			}
				
			public System.String ProductVariantID
			{
				get
				{
					System.Guid? data = entity.ProductVariantID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ProductVariantID = null;
					else entity.ProductVariantID = new Guid(value);
				}
			}
				
			public System.String CardExpiryDate
			{
				get
				{
					System.DateTime? data = entity.CardExpiryDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CardExpiryDate = null;
					else entity.CardExpiryDate = Convert.ToDateTime(value);
				}
			}
				
			public System.String CalcProcessStatus
			{
				get
				{
					System.Char? data = entity.CalcProcessStatus;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalcProcessStatus = null;
					else entity.CalcProcessStatus = Convert.ToChar(value);
				}
			}
				
			public System.String TrxID
			{
				get
				{
					System.Int64? data = entity.TrxID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TrxID = null;
					else entity.TrxID = Convert.ToInt64(value);
				}
			}
				
			public System.String UpdLoyaltyStatus
			{
				get
				{
					System.Char? data = entity.UpdLoyaltyStatus;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UpdLoyaltyStatus = null;
					else entity.UpdLoyaltyStatus = Convert.ToChar(value);
				}
			}
				
			public System.String DateTimeLog
			{
				get
				{
					System.DateTime? data = entity.DateTimeLog;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DateTimeLog = null;
					else entity.DateTimeLog = Convert.ToDateTime(value);
				}
			}
				
			public System.String TempStatus
			{
				get
				{
					System.Int32? data = entity.TempStatus;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TempStatus = null;
					else entity.TempStatus = Convert.ToInt32(value);
				}
			}
				
			public System.String SettlementDate
			{
				get
				{
					System.DateTime? data = entity.SettlementDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SettlementDate = null;
					else entity.SettlementDate = Convert.ToDateTime(value);
				}
			}
				
			public System.String DecRebate
			{
				get
				{
					System.Decimal? data = entity.DecRebate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DecRebate = null;
					else entity.DecRebate = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdRebate
			{
				get
				{
					System.Int64? data = entity.IdRebate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRebate = null;
					else entity.IdRebate = Convert.ToInt64(value);
				}
			}
			

			private esTblFestiveRebateTrx entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TblFestiveRebateTrxMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TblFestiveRebateTrxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TblFestiveRebateTrxQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TblFestiveRebateTrxQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TblFestiveRebateTrxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}

		#endregion
		
        [IgnoreDataMember]
		private TblFestiveRebateTrxQuery query;		
	}



	[Serializable]
	abstract public partial class esTblFestiveRebateTrxCollection : esEntityCollection<TblFestiveRebateTrx>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TblFestiveRebateTrxMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TblFestiveRebateTrxCollection";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TblFestiveRebateTrxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TblFestiveRebateTrxQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TblFestiveRebateTrxQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TblFestiveRebateTrxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TblFestiveRebateTrxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TblFestiveRebateTrxQuery)query);
		}

		#endregion
		
		private TblFestiveRebateTrxQuery query;
	}



	[Serializable]
	abstract public partial class esTblFestiveRebateTrxQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TblFestiveRebateTrxMetadata.Meta();
			}
		}	
		
		#region QueryItemFromName
		
        protected override esQueryItem QueryItemFromName(string name)
        {
            switch (name)
            {
				case "TrxDate": return this.TrxDate;
				case "TrxType": return this.TrxType;
				case "TrxCode": return this.TrxCode;
				case "MCCCode": return this.MCCCode;
				case "MerchantID": return this.MerchantID;
				case "MerchantName": return this.MerchantName;
				case "TrxCurrCode": return this.TrxCurrCode;
				case "BillCurrCode": return this.BillCurrCode;
				case "TrxAmt": return this.TrxAmt;
				case "CountryCode": return this.CountryCode;
				case "POSMode": return this.POSMode;
				case "CardNo": return this.CardNo;
				case "BillAmt": return this.BillAmt;
				case "RefNo": return this.RefNo;
				case "AuthCode": return this.AuthCode;
				case "Remarks": return this.Remarks;
				case "TrxParamXML": return this.TrxParamXML;
				case "UpdXMLStatus": return this.UpdXMLStatus;
				case "BatchID": return this.BatchID;
				case "CardNoShow": return this.CardNoShow;
				case "MemberID": return this.MemberID;
				case "MemberTypeID": return this.MemberTypeID;
				case "ProductVariantID": return this.ProductVariantID;
				case "CardExpiryDate": return this.CardExpiryDate;
				case "CalcProcessStatus": return this.CalcProcessStatus;
				case "TrxID": return this.TrxID;
				case "UpdLoyaltyStatus": return this.UpdLoyaltyStatus;
				case "DateTimeLog": return this.DateTimeLog;
				case "TempStatus": return this.TempStatus;
				case "SettlementDate": return this.SettlementDate;
				case "DecRebate": return this.DecRebate;
				case "IdRebate": return this.IdRebate;

                default: return null;
            }
        }		
		
		#endregion
		
		#region esQueryItems

		public esQueryItem TrxDate
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.TrxDate, esSystemType.DateTime); }
		} 
		
		public esQueryItem TrxType
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.TrxType, esSystemType.Char); }
		} 
		
		public esQueryItem TrxCode
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.TrxCode, esSystemType.String); }
		} 
		
		public esQueryItem MCCCode
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.MCCCode, esSystemType.String); }
		} 
		
		public esQueryItem MerchantID
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.MerchantID, esSystemType.String); }
		} 
		
		public esQueryItem MerchantName
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.MerchantName, esSystemType.String); }
		} 
		
		public esQueryItem TrxCurrCode
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.TrxCurrCode, esSystemType.String); }
		} 
		
		public esQueryItem BillCurrCode
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.BillCurrCode, esSystemType.String); }
		} 
		
		public esQueryItem TrxAmt
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.TrxAmt, esSystemType.Decimal); }
		} 
		
		public esQueryItem CountryCode
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.CountryCode, esSystemType.String); }
		} 
		
		public esQueryItem POSMode
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.POSMode, esSystemType.String); }
		} 
		
		public esQueryItem CardNo
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.CardNo, esSystemType.String); }
		} 
		
		public esQueryItem BillAmt
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.BillAmt, esSystemType.Decimal); }
		} 
		
		public esQueryItem RefNo
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.RefNo, esSystemType.String); }
		} 
		
		public esQueryItem AuthCode
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.AuthCode, esSystemType.String); }
		} 
		
		public esQueryItem Remarks
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.Remarks, esSystemType.String); }
		} 
		
		public esQueryItem TrxParamXML
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.TrxParamXML, esSystemType.String); }
		} 
		
		public esQueryItem UpdXMLStatus
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.UpdXMLStatus, esSystemType.Boolean); }
		} 
		
		public esQueryItem BatchID
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.BatchID, esSystemType.String); }
		} 
		
		public esQueryItem CardNoShow
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.CardNoShow, esSystemType.String); }
		} 
		
		public esQueryItem MemberID
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.MemberID, esSystemType.Guid); }
		} 
		
		public esQueryItem MemberTypeID
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.MemberTypeID, esSystemType.Guid); }
		} 
		
		public esQueryItem ProductVariantID
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.ProductVariantID, esSystemType.Guid); }
		} 
		
		public esQueryItem CardExpiryDate
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.CardExpiryDate, esSystemType.DateTime); }
		} 
		
		public esQueryItem CalcProcessStatus
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.CalcProcessStatus, esSystemType.Char); }
		} 
		
		public esQueryItem TrxID
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.TrxID, esSystemType.Int64); }
		} 
		
		public esQueryItem UpdLoyaltyStatus
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.UpdLoyaltyStatus, esSystemType.Char); }
		} 
		
		public esQueryItem DateTimeLog
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.DateTimeLog, esSystemType.DateTime); }
		} 
		
		public esQueryItem TempStatus
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.TempStatus, esSystemType.Int32); }
		} 
		
		public esQueryItem SettlementDate
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.SettlementDate, esSystemType.DateTime); }
		} 
		
		public esQueryItem DecRebate
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.DecRebate, esSystemType.Decimal); }
		} 
		
		public esQueryItem IdRebate
		{
			get { return new esQueryItem(this, TblFestiveRebateTrxMetadata.ColumnNames.IdRebate, esSystemType.Int64); }
		} 
		
		#endregion
		
	}


	
	public partial class TblFestiveRebateTrx : esTblFestiveRebateTrx
	{

		
		
	}
	



	[Serializable]
	public partial class TblFestiveRebateTrxMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TblFestiveRebateTrxMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.TrxDate, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.TrxDate;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.TrxType, 1, typeof(System.Char), esSystemType.Char);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.TrxType;
			c.CharacterMaxLength = 1;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.TrxCode, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.TrxCode;
			c.CharacterMaxLength = 4;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.MCCCode, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.MCCCode;
			c.CharacterMaxLength = 5;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.MerchantID, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.MerchantID;
			c.CharacterMaxLength = 12;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.MerchantName, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.MerchantName;
			c.CharacterMaxLength = 40;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.TrxCurrCode, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.TrxCurrCode;
			c.CharacterMaxLength = 3;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.BillCurrCode, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.BillCurrCode;
			c.CharacterMaxLength = 3;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.TrxAmt, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.TrxAmt;
			c.NumericPrecision = 12;
			c.NumericScale = 2;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.CountryCode, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.CountryCode;
			c.CharacterMaxLength = 3;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.POSMode, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.POSMode;
			c.CharacterMaxLength = 2;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.CardNo, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.CardNo;
			c.CharacterMaxLength = 50;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.BillAmt, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.BillAmt;
			c.NumericPrecision = 12;
			c.NumericScale = 2;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.RefNo, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.RefNo;
			c.CharacterMaxLength = 20;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.AuthCode, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.AuthCode;
			c.CharacterMaxLength = 6;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.Remarks, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.Remarks;
			c.CharacterMaxLength = 20;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.TrxParamXML, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.TrxParamXML;
			c.CharacterMaxLength = 8000;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.UpdXMLStatus, 17, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.UpdXMLStatus;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.BatchID, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.BatchID;
			c.CharacterMaxLength = 50;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.CardNoShow, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.CardNoShow;
			c.CharacterMaxLength = 20;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.MemberID, 20, typeof(System.Guid), esSystemType.Guid);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.MemberID;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.MemberTypeID, 21, typeof(System.Guid), esSystemType.Guid);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.MemberTypeID;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.ProductVariantID, 22, typeof(System.Guid), esSystemType.Guid);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.ProductVariantID;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.CardExpiryDate, 23, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.CardExpiryDate;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.CalcProcessStatus, 24, typeof(System.Char), esSystemType.Char);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.CalcProcessStatus;
			c.CharacterMaxLength = 1;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.TrxID, 25, typeof(System.Int64), esSystemType.Int64);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.TrxID;
			c.NumericPrecision = 19;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.UpdLoyaltyStatus, 26, typeof(System.Char), esSystemType.Char);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.UpdLoyaltyStatus;
			c.CharacterMaxLength = 1;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.DateTimeLog, 27, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.DateTimeLog;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.TempStatus, 28, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.TempStatus;
			c.NumericPrecision = 10;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.SettlementDate, 29, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.SettlementDate;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.DecRebate, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.DecRebate;
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			c.IsNullable = true;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveRebateTrxMetadata.ColumnNames.IdRebate, 31, typeof(System.Int64), esSystemType.Int64);
			c.PropertyName = TblFestiveRebateTrxMetadata.PropertyNames.IdRebate;
			c.NumericPrecision = 19;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TblFestiveRebateTrxMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TrxDate = "TrxDate";
			 public const string TrxType = "TrxType";
			 public const string TrxCode = "TrxCode";
			 public const string MCCCode = "MCCCode";
			 public const string MerchantID = "MerchantID";
			 public const string MerchantName = "MerchantName";
			 public const string TrxCurrCode = "TrxCurrCode";
			 public const string BillCurrCode = "BillCurrCode";
			 public const string TrxAmt = "TrxAmt";
			 public const string CountryCode = "CountryCode";
			 public const string POSMode = "POSMode";
			 public const string CardNo = "CardNo";
			 public const string BillAmt = "BillAmt";
			 public const string RefNo = "RefNo";
			 public const string AuthCode = "AuthCode";
			 public const string Remarks = "Remarks";
			 public const string TrxParamXML = "TrxParamXML";
			 public const string UpdXMLStatus = "UpdXMLStatus";
			 public const string BatchID = "BatchID";
			 public const string CardNoShow = "CardNoShow";
			 public const string MemberID = "MemberID";
			 public const string MemberTypeID = "MemberTypeID";
			 public const string ProductVariantID = "ProductVariantID";
			 public const string CardExpiryDate = "CardExpiryDate";
			 public const string CalcProcessStatus = "CalcProcessStatus";
			 public const string TrxID = "TrxID";
			 public const string UpdLoyaltyStatus = "UpdLoyaltyStatus";
			 public const string DateTimeLog = "DateTimeLog";
			 public const string TempStatus = "TempStatus";
			 public const string SettlementDate = "SettlementDate";
			 public const string DecRebate = "decRebate";
			 public const string IdRebate = "idRebate";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TrxDate = "TrxDate";
			 public const string TrxType = "TrxType";
			 public const string TrxCode = "TrxCode";
			 public const string MCCCode = "MCCCode";
			 public const string MerchantID = "MerchantID";
			 public const string MerchantName = "MerchantName";
			 public const string TrxCurrCode = "TrxCurrCode";
			 public const string BillCurrCode = "BillCurrCode";
			 public const string TrxAmt = "TrxAmt";
			 public const string CountryCode = "CountryCode";
			 public const string POSMode = "POSMode";
			 public const string CardNo = "CardNo";
			 public const string BillAmt = "BillAmt";
			 public const string RefNo = "RefNo";
			 public const string AuthCode = "AuthCode";
			 public const string Remarks = "Remarks";
			 public const string TrxParamXML = "TrxParamXML";
			 public const string UpdXMLStatus = "UpdXMLStatus";
			 public const string BatchID = "BatchID";
			 public const string CardNoShow = "CardNoShow";
			 public const string MemberID = "MemberID";
			 public const string MemberTypeID = "MemberTypeID";
			 public const string ProductVariantID = "ProductVariantID";
			 public const string CardExpiryDate = "CardExpiryDate";
			 public const string CalcProcessStatus = "CalcProcessStatus";
			 public const string TrxID = "TrxID";
			 public const string UpdLoyaltyStatus = "UpdLoyaltyStatus";
			 public const string DateTimeLog = "DateTimeLog";
			 public const string TempStatus = "TempStatus";
			 public const string SettlementDate = "SettlementDate";
			 public const string DecRebate = "DecRebate";
			 public const string IdRebate = "IdRebate";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TblFestiveRebateTrxMetadata))
			{
				if(TblFestiveRebateTrxMetadata.mapDelegates == null)
				{
					TblFestiveRebateTrxMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TblFestiveRebateTrxMetadata.meta == null)
				{
					TblFestiveRebateTrxMetadata.meta = new TblFestiveRebateTrxMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("TrxDate", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TrxType", new esTypeMap("varchar", "System.Char"));
				meta.AddTypeMap("TrxCode", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("MCCCode", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("MerchantID", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("MerchantName", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TrxCurrCode", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("BillCurrCode", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TrxAmt", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CountryCode", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("POSMode", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CardNo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("BillAmt", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RefNo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("AuthCode", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Remarks", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TrxParamXML", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("UpdXMLStatus", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("BatchID", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CardNoShow", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("MemberID", new esTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("MemberTypeID", new esTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("ProductVariantID", new esTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("CardExpiryDate", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CalcProcessStatus", new esTypeMap("varchar", "System.Char"));
				meta.AddTypeMap("TrxID", new esTypeMap("bigint", "System.Int64"));
				meta.AddTypeMap("UpdLoyaltyStatus", new esTypeMap("varchar", "System.Char"));
				meta.AddTypeMap("DateTimeLog", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TempStatus", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("SettlementDate", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DecRebate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdRebate", new esTypeMap("bigint", "System.Int64"));			
				
				
				
				meta.Source = "tblFestiveRebateTrx";
				meta.Destination = "tblFestiveRebateTrx";
				
				meta.spInsert = "proc_tblFestiveRebateTrxInsert";				
				meta.spUpdate = "proc_tblFestiveRebateTrxUpdate";		
				meta.spDelete = "proc_tblFestiveRebateTrxDelete";
				meta.spLoadAll = "proc_tblFestiveRebateTrxLoadAll";
				meta.spLoadByPrimaryKey = "proc_tblFestiveRebateTrxLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TblFestiveRebateTrxMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}

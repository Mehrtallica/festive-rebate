
/*
===============================================================================
                    EntitySpaces Studio by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 6/16/2017 10:30:24 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace DAL
{
	/// <summary>
	/// Encapsulates the 'tblSetting' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[KnownType(typeof(TblSetting))]	
	[XmlType("TblSetting")]
	public partial class TblSetting : esTblSetting
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TblSetting();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Guid id)
		{
			var obj = new TblSetting();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Guid id, esSqlAccessType sqlAccessType)
		{
			var obj = new TblSetting();
			obj.Id = id;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion

		
		
		override protected string GetConnectionName()
		{
			return "root";
		}			
		
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TblSettingCollection")]
	public partial class TblSettingCollection : esTblSettingCollection, IEnumerable<TblSetting>
	{
		public TblSetting FindByPrimaryKey(System.Guid id)
		{
			return this.SingleOrDefault(e => e.Id == id);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TblSetting))]
		public class TblSettingCollectionWCFPacket : esCollectionWCFPacket<TblSettingCollection>
		{
			public static implicit operator TblSettingCollection(TblSettingCollectionWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TblSettingCollectionWCFPacket(TblSettingCollection collection)
			{
				return new TblSettingCollectionWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "root";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TblSettingQuery : esTblSettingQuery
	{
		public TblSettingQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	

		override protected string GetQueryName()
		{
			return "TblSettingQuery";
		}
		
		
		override protected string GetConnectionName()
		{
			return "root";
		}			
	
		#region Explicit Casts
		
		public static explicit operator string(TblSettingQuery query)
		{
			return TblSettingQuery.SerializeHelper.ToXml(query);
		}

		public static explicit operator TblSettingQuery(string query)
		{
			return (TblSettingQuery)TblSettingQuery.SerializeHelper.FromXml(query, typeof(TblSettingQuery));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTblSetting : esEntity
	{
		public esTblSetting()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Guid id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Guid id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Guid id)
		{
			TblSettingQuery query = new TblSettingQuery();
			query.Where(query.Id == id);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Guid id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id", id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to tblSetting.ID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Guid? Id
		{
			get
			{
				return base.GetSystemGuid(TblSettingMetadata.ColumnNames.Id);
			}
			
			set
			{
				if(base.SetSystemGuid(TblSettingMetadata.ColumnNames.Id, value))
				{
					OnPropertyChanged(TblSettingMetadata.PropertyNames.Id);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblSetting.RebateID
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int64? RebateID
		{
			get
			{
				return base.GetSystemInt64(TblSettingMetadata.ColumnNames.RebateID);
			}
			
			set
			{
				if(base.SetSystemInt64(TblSettingMetadata.ColumnNames.RebateID, value))
				{
					OnPropertyChanged(TblSettingMetadata.PropertyNames.RebateID);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblSetting.EntitleDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? EntitleDate
		{
			get
			{
				return base.GetSystemDateTime(TblSettingMetadata.ColumnNames.EntitleDate);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblSettingMetadata.ColumnNames.EntitleDate, value))
				{
					OnPropertyChanged(TblSettingMetadata.PropertyNames.EntitleDate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblSetting.EffectiveDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? EffectiveDate
		{
			get
			{
				return base.GetSystemDateTime(TblSettingMetadata.ColumnNames.EffectiveDate);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblSettingMetadata.ColumnNames.EffectiveDate, value))
				{
					OnPropertyChanged(TblSettingMetadata.PropertyNames.EffectiveDate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblSetting.ExpiryDate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? ExpiryDate
		{
			get
			{
				return base.GetSystemDateTime(TblSettingMetadata.ColumnNames.ExpiryDate);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblSettingMetadata.ColumnNames.ExpiryDate, value))
				{
					OnPropertyChanged(TblSettingMetadata.PropertyNames.ExpiryDate);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str().Id = (string)value; break;							
						case "RebateID": this.str().RebateID = (string)value; break;							
						case "EntitleDate": this.str().EntitleDate = (string)value; break;							
						case "EffectiveDate": this.str().EffectiveDate = (string)value; break;							
						case "ExpiryDate": this.str().ExpiryDate = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value is System.Guid)
								this.Id = (System.Guid?)value;
								OnPropertyChanged(TblSettingMetadata.PropertyNames.Id);
							break;
						
						case "RebateID":
						
							if (value == null || value is System.Int64)
								this.RebateID = (System.Int64?)value;
								OnPropertyChanged(TblSettingMetadata.PropertyNames.RebateID);
							break;
						
						case "EntitleDate":
						
							if (value == null || value is System.DateTime)
								this.EntitleDate = (System.DateTime?)value;
								OnPropertyChanged(TblSettingMetadata.PropertyNames.EntitleDate);
							break;
						
						case "EffectiveDate":
						
							if (value == null || value is System.DateTime)
								this.EffectiveDate = (System.DateTime?)value;
								OnPropertyChanged(TblSettingMetadata.PropertyNames.EffectiveDate);
							break;
						
						case "ExpiryDate":
						
							if (value == null || value is System.DateTime)
								this.ExpiryDate = (System.DateTime?)value;
								OnPropertyChanged(TblSettingMetadata.PropertyNames.ExpiryDate);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTblSetting entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Guid? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = new Guid(value);
				}
			}
				
			public System.String RebateID
			{
				get
				{
					System.Int64? data = entity.RebateID;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebateID = null;
					else entity.RebateID = Convert.ToInt64(value);
				}
			}
				
			public System.String EntitleDate
			{
				get
				{
					System.DateTime? data = entity.EntitleDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EntitleDate = null;
					else entity.EntitleDate = Convert.ToDateTime(value);
				}
			}
				
			public System.String EffectiveDate
			{
				get
				{
					System.DateTime? data = entity.EffectiveDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EffectiveDate = null;
					else entity.EffectiveDate = Convert.ToDateTime(value);
				}
			}
				
			public System.String ExpiryDate
			{
				get
				{
					System.DateTime? data = entity.ExpiryDate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExpiryDate = null;
					else entity.ExpiryDate = Convert.ToDateTime(value);
				}
			}
			

			private esTblSetting entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TblSettingMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TblSettingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TblSettingQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TblSettingQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TblSettingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}

		#endregion
		
        [IgnoreDataMember]
		private TblSettingQuery query;		
	}



	[Serializable]
	abstract public partial class esTblSettingCollection : esEntityCollection<TblSetting>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TblSettingMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TblSettingCollection";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TblSettingQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TblSettingQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TblSettingQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TblSettingQuery();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TblSettingQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TblSettingQuery)query);
		}

		#endregion
		
		private TblSettingQuery query;
	}



	[Serializable]
	abstract public partial class esTblSettingQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TblSettingMetadata.Meta();
			}
		}	
		
		#region QueryItemFromName
		
        protected override esQueryItem QueryItemFromName(string name)
        {
            switch (name)
            {
				case "Id": return this.Id;
				case "RebateID": return this.RebateID;
				case "EntitleDate": return this.EntitleDate;
				case "EffectiveDate": return this.EffectiveDate;
				case "ExpiryDate": return this.ExpiryDate;

                default: return null;
            }
        }		
		
		#endregion
		
		#region esQueryItems

		public esQueryItem Id
		{
			get { return new esQueryItem(this, TblSettingMetadata.ColumnNames.Id, esSystemType.Guid); }
		} 
		
		public esQueryItem RebateID
		{
			get { return new esQueryItem(this, TblSettingMetadata.ColumnNames.RebateID, esSystemType.Int64); }
		} 
		
		public esQueryItem EntitleDate
		{
			get { return new esQueryItem(this, TblSettingMetadata.ColumnNames.EntitleDate, esSystemType.DateTime); }
		} 
		
		public esQueryItem EffectiveDate
		{
			get { return new esQueryItem(this, TblSettingMetadata.ColumnNames.EffectiveDate, esSystemType.DateTime); }
		} 
		
		public esQueryItem ExpiryDate
		{
			get { return new esQueryItem(this, TblSettingMetadata.ColumnNames.ExpiryDate, esSystemType.DateTime); }
		} 
		
		#endregion
		
	}


	
	public partial class TblSetting : esTblSetting
	{

		
		
	}
	



	[Serializable]
	public partial class TblSettingMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TblSettingMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TblSettingMetadata.ColumnNames.Id, 0, typeof(System.Guid), esSystemType.Guid);
			c.PropertyName = TblSettingMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;
			c.HasDefault = true;
			c.Default = @"(newid())";
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblSettingMetadata.ColumnNames.RebateID, 1, typeof(System.Int64), esSystemType.Int64);
			c.PropertyName = TblSettingMetadata.PropertyNames.RebateID;
			c.NumericPrecision = 19;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblSettingMetadata.ColumnNames.EntitleDate, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblSettingMetadata.PropertyNames.EntitleDate;
			c.CharacterMaxLength = 10;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblSettingMetadata.ColumnNames.EffectiveDate, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblSettingMetadata.PropertyNames.EffectiveDate;
			c.CharacterMaxLength = 10;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblSettingMetadata.ColumnNames.ExpiryDate, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblSettingMetadata.PropertyNames.ExpiryDate;
			c.CharacterMaxLength = 10;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TblSettingMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "ID";
			 public const string RebateID = "RebateID";
			 public const string EntitleDate = "EntitleDate";
			 public const string EffectiveDate = "EffectiveDate";
			 public const string ExpiryDate = "ExpiryDate";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string RebateID = "RebateID";
			 public const string EntitleDate = "EntitleDate";
			 public const string EffectiveDate = "EffectiveDate";
			 public const string ExpiryDate = "ExpiryDate";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TblSettingMetadata))
			{
				if(TblSettingMetadata.mapDelegates == null)
				{
					TblSettingMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TblSettingMetadata.meta == null)
				{
					TblSettingMetadata.meta = new TblSettingMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("Id", new esTypeMap("uniqueidentifier", "System.Guid"));
				meta.AddTypeMap("RebateID", new esTypeMap("bigint", "System.Int64"));
				meta.AddTypeMap("EntitleDate", new esTypeMap("date", "System.DateTime"));
				meta.AddTypeMap("EffectiveDate", new esTypeMap("date", "System.DateTime"));
				meta.AddTypeMap("ExpiryDate", new esTypeMap("date", "System.DateTime"));			
				
				
				
				meta.Source = "tblSetting";
				meta.Destination = "tblSetting";
				
				meta.spInsert = "proc_tblSettingInsert";				
				meta.spUpdate = "proc_tblSettingUpdate";		
				meta.spDelete = "proc_tblSettingDelete";
				meta.spLoadAll = "proc_tblSettingLoadAll";
				meta.spLoadByPrimaryKey = "proc_tblSettingLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TblSettingMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}

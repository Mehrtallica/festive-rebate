
/*
===============================================================================
                    EntitySpaces Studio by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2012.1.0930.0
EntitySpaces Driver  : SQL
Date Generated       : 6/15/2017 11:33:29 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using EntitySpaces.DynamicQuery;



namespace DAL
{
	/// <summary>
	/// Encapsulates the 'tblFestive' table
	/// </summary>

    [DebuggerDisplay("Data = {Debug}")]
	[Serializable]
	[DataContract]
	[KnownType(typeof(TblFestive))]	
	[XmlType("TblFestive")]
	public partial class TblFestive : esTblFestive
	{	
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden | DebuggerBrowsableState.Never)]
		protected override esEntityDebuggerView[] Debug
		{
			get { return base.Debug; }
		}

		override public esEntity CreateInstance()
		{
			return new TblFestive();
		}
		
		#region Static Quick Access Methods
		static public void Delete(System.Int64 idRebate)
		{
			var obj = new TblFestive();
			obj.IdRebate = idRebate;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save();
		}

	    static public void Delete(System.Int64 idRebate, esSqlAccessType sqlAccessType)
		{
			var obj = new TblFestive();
			obj.IdRebate = idRebate;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
			obj.Save(sqlAccessType);
		}
		#endregion

		
		
		override protected string GetConnectionName()
		{
			return "root";
		}			
		
	
	}



    [DebuggerDisplay("Count = {Count}")]
	[Serializable]
	[CollectionDataContract]
	[XmlType("TblFestiveCollection")]
	public partial class TblFestiveCollection : esTblFestiveCollection, IEnumerable<TblFestive>
	{
		public TblFestive FindByPrimaryKey(System.Int64 idRebate)
		{
			return this.SingleOrDefault(e => e.IdRebate == idRebate);
		}

		
		
		#region WCF Service Class
		
		[DataContract]
		[KnownType(typeof(TblFestive))]
		public class TblFestiveCollectionWCFPacket : esCollectionWCFPacket<TblFestiveCollection>
		{
			public static implicit operator TblFestiveCollection(TblFestiveCollectionWCFPacket packet)
			{
				return packet.Collection;
			}

			public static implicit operator TblFestiveCollectionWCFPacket(TblFestiveCollection collection)
			{
				return new TblFestiveCollectionWCFPacket() { Collection = collection };
			}
		}
		
		#endregion
		
		
		override protected string GetConnectionName()
		{
			return "root";
		}		
	}



    [DebuggerDisplay("Query = {Parse()}")]
	[Serializable]	
	public partial class TblFestiveQuery : esTblFestiveQuery
	{
		public TblFestiveQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	

		override protected string GetQueryName()
		{
			return "TblFestiveQuery";
		}
		
		
		override protected string GetConnectionName()
		{
			return "root";
		}			
	
		#region Explicit Casts
		
		public static explicit operator string(TblFestiveQuery query)
		{
			return TblFestiveQuery.SerializeHelper.ToXml(query);
		}

		public static explicit operator TblFestiveQuery(string query)
		{
			return (TblFestiveQuery)TblFestiveQuery.SerializeHelper.FromXml(query, typeof(TblFestiveQuery));
		}
		
		#endregion		
	}

	[DataContract]
	[Serializable]
	abstract public partial class esTblFestive : esEntity
	{
		public esTblFestive()
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int64 idRebate)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRebate);
			else
				return LoadByPrimaryKeyStoredProcedure(idRebate);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int64 idRebate)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRebate);
			else
				return LoadByPrimaryKeyStoredProcedure(idRebate);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int64 idRebate)
		{
			TblFestiveQuery query = new TblFestiveQuery();
			query.Where(query.IdRebate == idRebate);
			return this.Load(query);
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int64 idRebate)
		{
			esParameters parms = new esParameters();
			parms.Add("IdRebate", idRebate);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		#region Properties
		
		
		
		/// <summary>
		/// Maps to tblFestive.idRebate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Int64? IdRebate
		{
			get
			{
				return base.GetSystemInt64(TblFestiveMetadata.ColumnNames.IdRebate);
			}
			
			set
			{
				if(base.SetSystemInt64(TblFestiveMetadata.ColumnNames.IdRebate, value))
				{
					OnPropertyChanged(TblFestiveMetadata.PropertyNames.IdRebate);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestive.dtStart
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? DtStart
		{
			get
			{
				return base.GetSystemDateTime(TblFestiveMetadata.ColumnNames.DtStart);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblFestiveMetadata.ColumnNames.DtStart, value))
				{
					OnPropertyChanged(TblFestiveMetadata.PropertyNames.DtStart);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestive.dtEnd
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.DateTime? DtEnd
		{
			get
			{
				return base.GetSystemDateTime(TblFestiveMetadata.ColumnNames.DtEnd);
			}
			
			set
			{
				if(base.SetSystemDateTime(TblFestiveMetadata.ColumnNames.DtEnd, value))
				{
					OnPropertyChanged(TblFestiveMetadata.PropertyNames.DtEnd);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestive.decMinTrxAmt
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? DecMinTrxAmt
		{
			get
			{
				return base.GetSystemDecimal(TblFestiveMetadata.ColumnNames.DecMinTrxAmt);
			}
			
			set
			{
				if(base.SetSystemDecimal(TblFestiveMetadata.ColumnNames.DecMinTrxAmt, value))
				{
					OnPropertyChanged(TblFestiveMetadata.PropertyNames.DecMinTrxAmt);
				}
			}
		}		
		
		/// <summary>
		/// Maps to tblFestive.decRebate
		/// </summary>
		[DataMember(EmitDefaultValue=false)]
		virtual public System.Decimal? DecRebate
		{
			get
			{
				return base.GetSystemDecimal(TblFestiveMetadata.ColumnNames.DecRebate);
			}
			
			set
			{
				if(base.SetSystemDecimal(TblFestiveMetadata.ColumnNames.DecRebate, value))
				{
					OnPropertyChanged(TblFestiveMetadata.PropertyNames.DecRebate);
				}
			}
		}		
		
		#endregion	

		#region .str() Properties
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}
		
		public override void SetProperty(string name, object value)
		{
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value is System.String)
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdRebate": this.str().IdRebate = (string)value; break;							
						case "DtStart": this.str().DtStart = (string)value; break;							
						case "DtEnd": this.str().DtEnd = (string)value; break;							
						case "DecMinTrxAmt": this.str().DecMinTrxAmt = (string)value; break;							
						case "DecRebate": this.str().DecRebate = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdRebate":
						
							if (value == null || value is System.Int64)
								this.IdRebate = (System.Int64?)value;
								OnPropertyChanged(TblFestiveMetadata.PropertyNames.IdRebate);
							break;
						
						case "DtStart":
						
							if (value == null || value is System.DateTime)
								this.DtStart = (System.DateTime?)value;
								OnPropertyChanged(TblFestiveMetadata.PropertyNames.DtStart);
							break;
						
						case "DtEnd":
						
							if (value == null || value is System.DateTime)
								this.DtEnd = (System.DateTime?)value;
								OnPropertyChanged(TblFestiveMetadata.PropertyNames.DtEnd);
							break;
						
						case "DecMinTrxAmt":
						
							if (value == null || value is System.Decimal)
								this.DecMinTrxAmt = (System.Decimal?)value;
								OnPropertyChanged(TblFestiveMetadata.PropertyNames.DecMinTrxAmt);
							break;
						
						case "DecRebate":
						
							if (value == null || value is System.Decimal)
								this.DecRebate = (System.Decimal?)value;
								OnPropertyChanged(TblFestiveMetadata.PropertyNames.DecRebate);
							break;
					

						default:
							break;
					}
				}
			}
            else if (this.ContainsColumn(name))
            {
                this.SetColumn(name, value);
            }
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}		

		public esStrings str()
		{
			if (esstrings == null)
			{
				esstrings = new esStrings(this);
			}
			return esstrings;
		}

		sealed public class esStrings
		{
			public esStrings(esTblFestive entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdRebate
			{
				get
				{
					System.Int64? data = entity.IdRebate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRebate = null;
					else entity.IdRebate = Convert.ToInt64(value);
				}
			}
				
			public System.String DtStart
			{
				get
				{
					System.DateTime? data = entity.DtStart;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtStart = null;
					else entity.DtStart = Convert.ToDateTime(value);
				}
			}
				
			public System.String DtEnd
			{
				get
				{
					System.DateTime? data = entity.DtEnd;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtEnd = null;
					else entity.DtEnd = Convert.ToDateTime(value);
				}
			}
				
			public System.String DecMinTrxAmt
			{
				get
				{
					System.Decimal? data = entity.DecMinTrxAmt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DecMinTrxAmt = null;
					else entity.DecMinTrxAmt = Convert.ToDecimal(value);
				}
			}
				
			public System.String DecRebate
			{
				get
				{
					System.Decimal? data = entity.DecRebate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DecRebate = null;
					else entity.DecRebate = Convert.ToDecimal(value);
				}
			}
			

			private esTblFestive entity;
		}
		
		[NonSerialized]
		private esStrings esstrings;		
		
		#endregion
		
		#region Housekeeping methods

		override protected IMetadata Meta
		{
			get
			{
				return TblFestiveMetadata.Meta();
			}
		}

		#endregion		
		
		#region Query Logic

		public TblFestiveQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TblFestiveQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TblFestiveQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return this.Query.Load();
		}
		
		protected void InitQuery(TblFestiveQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntity)this).Connection;
			}			
		}

		#endregion
		
        [IgnoreDataMember]
		private TblFestiveQuery query;		
	}



	[Serializable]
	abstract public partial class esTblFestiveCollection : esEntityCollection<TblFestive>
	{
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TblFestiveMetadata.Meta();
			}
		}

		protected override string GetCollectionName()
		{
			return "TblFestiveCollection";
		}

		#endregion		
		
		#region Query Logic

	#if (!WindowsCE)
		[BrowsableAttribute(false)]
	#endif
		public TblFestiveQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TblFestiveQuery();
					InitQuery(this.query);
				}

				return this.query;
			}
		}

		public bool Load(TblFestiveQuery query)
		{
			this.query = query;
			InitQuery(this.query);
			return Query.Load();
		}

		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TblFestiveQuery();
				this.InitQuery(query);
			}
			return this.query;
		}

		protected void InitQuery(TblFestiveQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			
			if (!query.es2.HasConnection)
			{
				query.es2.Connection = ((IEntityCollection)this).Connection;
			}			
		}

		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery((TblFestiveQuery)query);
		}

		#endregion
		
		private TblFestiveQuery query;
	}



	[Serializable]
	abstract public partial class esTblFestiveQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TblFestiveMetadata.Meta();
			}
		}	
		
		#region QueryItemFromName
		
        protected override esQueryItem QueryItemFromName(string name)
        {
            switch (name)
            {
				case "IdRebate": return this.IdRebate;
				case "DtStart": return this.DtStart;
				case "DtEnd": return this.DtEnd;
				case "DecMinTrxAmt": return this.DecMinTrxAmt;
				case "DecRebate": return this.DecRebate;

                default: return null;
            }
        }		
		
		#endregion
		
		#region esQueryItems

		public esQueryItem IdRebate
		{
			get { return new esQueryItem(this, TblFestiveMetadata.ColumnNames.IdRebate, esSystemType.Int64); }
		} 
		
		public esQueryItem DtStart
		{
			get { return new esQueryItem(this, TblFestiveMetadata.ColumnNames.DtStart, esSystemType.DateTime); }
		} 
		
		public esQueryItem DtEnd
		{
			get { return new esQueryItem(this, TblFestiveMetadata.ColumnNames.DtEnd, esSystemType.DateTime); }
		} 
		
		public esQueryItem DecMinTrxAmt
		{
			get { return new esQueryItem(this, TblFestiveMetadata.ColumnNames.DecMinTrxAmt, esSystemType.Decimal); }
		} 
		
		public esQueryItem DecRebate
		{
			get { return new esQueryItem(this, TblFestiveMetadata.ColumnNames.DecRebate, esSystemType.Decimal); }
		} 
		
		#endregion
		
	}


	
	public partial class TblFestive : esTblFestive
	{

		
		
	}
	



	[Serializable]
	public partial class TblFestiveMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TblFestiveMetadata()
		{
			m_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TblFestiveMetadata.ColumnNames.IdRebate, 0, typeof(System.Int64), esSystemType.Int64);
			c.PropertyName = TblFestiveMetadata.PropertyNames.IdRebate;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;
			c.NumericPrecision = 19;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveMetadata.ColumnNames.DtStart, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblFestiveMetadata.PropertyNames.DtStart;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveMetadata.ColumnNames.DtEnd, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TblFestiveMetadata.PropertyNames.DtEnd;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveMetadata.ColumnNames.DecMinTrxAmt, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TblFestiveMetadata.PropertyNames.DecMinTrxAmt;
			c.NumericPrecision = 9;
			c.NumericScale = 2;
			m_columns.Add(c);
				
			c = new esColumnMetadata(TblFestiveMetadata.ColumnNames.DecRebate, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TblFestiveMetadata.PropertyNames.DecRebate;
			c.NumericPrecision = 9;
			c.NumericScale = 2;
			m_columns.Add(c);
				
		}
		#endregion	
	
		static public TblFestiveMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base.m_dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base.m_columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdRebate = "idRebate";
			 public const string DtStart = "dtStart";
			 public const string DtEnd = "dtEnd";
			 public const string DecMinTrxAmt = "decMinTrxAmt";
			 public const string DecRebate = "decRebate";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdRebate = "IdRebate";
			 public const string DtStart = "DtStart";
			 public const string DtEnd = "DtEnd";
			 public const string DecMinTrxAmt = "DecMinTrxAmt";
			 public const string DecRebate = "DecRebate";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TblFestiveMetadata))
			{
				if(TblFestiveMetadata.mapDelegates == null)
				{
					TblFestiveMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TblFestiveMetadata.meta == null)
				{
					TblFestiveMetadata.meta = new TblFestiveMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!m_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();			


				meta.AddTypeMap("IdRebate", new esTypeMap("bigint", "System.Int64"));
				meta.AddTypeMap("DtStart", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DtEnd", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DecMinTrxAmt", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DecRebate", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "tblFestive";
				meta.Destination = "tblFestive";
				
				meta.spInsert = "proc_tblFestiveInsert";				
				meta.spUpdate = "proc_tblFestiveUpdate";		
				meta.spDelete = "proc_tblFestiveDelete";
				meta.spLoadAll = "proc_tblFestiveLoadAll";
				meta.spLoadByPrimaryKey = "proc_tblFestiveLoadByPrimaryKey";
				
				this.m_providerMetadataMaps["esDefault"] = meta;
			}
			
			return this.m_providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TblFestiveMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}

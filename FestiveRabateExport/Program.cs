﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;
using FestiveRabateExport.Code;
using System.IO;
using System.Configuration;

namespace FestiveRabateExport
{
    class Program
    {
        static BackgroundWorker ExportThread = new BackgroundWorker();
        static int RebateID;
        static DateTime EntitleDate;
        static DateTime EffectiveDate;
        static DateTime ExpiryDate;

        static void Main(string[] args)
        {

            EntitySpaces.Interfaces.esProviderFactory.Factory =
        new EntitySpaces.Loader.esDataProviderFactory();
            ExportThread.DoWork += new DoWorkEventHandler(ExportThread_DoWork);
            ExportThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ExportThread_RunWorkerCompleted);

            try
            {
                //Console.WriteLine("Rebate ID? (3 digit characters)");
                //RebateID = int.Parse(Console.ReadLine().Trim());
                //if (RebateID != 0)
                if (!PropmtUser())
                    return;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                return;
            }

            ExportThread.RunWorkerAsync();
            new ManualResetEvent(false).WaitOne();
        }

        static void ExportThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
                Console.WriteLine(e.Error.Message);
            else
                Console.WriteLine("Exported Successfully");
        }

        static void ExportThread_DoWork(object sender, DoWorkEventArgs e)
        {
            Console.WriteLine("Started...");
            Console.WriteLine("Running SP...");
            //Shared.ExecuteNonQuery("EXEC spProcessFestiveTrx");
            Console.WriteLine("Completed SP...");
            Console.WriteLine("Exporting Data...");
            ExportDetail();
            Console.WriteLine("Exporting Completed...");

        }

        static void ExportDetail()
        {
            var data = new DAL.FestiveRebateEntitlementVWCollection();
            var splitcount = 1;
            var path = ConfigurationManager.AppSettings["ExportFolderPath"].ToString();
            var date = DateTime.Now;
            var filename = GenerateFileName(date,RebateID, splitcount);
            data.Query.SelectAll();
            if (!data.Query.Load())
            {
                Console.WriteLine("No data was loaded from database.");
                return;
            }

            var columns = data[0].GetCurrentListOfColumns();
            var lines = new StringBuilder("");
            var newline = new StringBuilder("");

            foreach (var item in data)
            {
                foreach (var col in columns)
                {
                    newline.Append(item.GetOriginalColumnValue(col));
                }

                var sz = decimal.Parse(ConfigurationManager.AppSettings["MaxSizeInMB"].ToString());
                if (GetSize(lines.ToString()) + GetSize(newline.ToString()) >= sz)
                {
                    splitcount += 1;
                    lines.Length--;
                    File.WriteAllText(path + filename, lines.ToString());
                    lines.Clear();
                    filename = GenerateFileName(date,RebateID, splitcount);
                }
                lines.AppendLine(newline.ToString());
                newline.Clear();
            }

            lines.Length--;
            File.WriteAllText(path + filename, lines.ToString());

            ExportHeader(date,RebateID, data.Count, splitcount);
        }

        static void ExportHeader(DateTime date,int RebateID, int totalcount, int splitcount)
        {
            var path = ConfigurationManager.AppSettings["ExportFolderPath"].ToString();
            var filename = GenerateFileName(date,RebateID, splitcount, true);
            string content = totalcount.ToString().PadLeft(10, '0') + splitcount.ToString().PadLeft(3, '0');
            File.WriteAllText(path + filename, content);
        }

        static string GenerateFileName(DateTime date,int RebateID, int splitcount, bool isheader = false)
        {
            if (!isheader)
                return "RBT_SUM_" + date.ToString("yyyyMMddHHmmss") + "_" + RebateID.ToString().PadLeft(4, '0') + "_" + splitcount.ToString().PadLeft(3, '0') + ".txt";
            else
                return "RBT_HDR_" + date.ToString("yyyyMMddHHmmss") + "_" + RebateID.ToString().PadLeft(4, '0') + ".txt";
        }

        static decimal GetSize(string str)
        {
            decimal megabyteSize = ((decimal)Encoding.ASCII.GetByteCount(str) / 1048576);
            return megabyteSize;
        }

        static bool PropmtUser()
        {
            //Console.WriteLine("Entitle Date? (YYYY-MM-DD)");
            //EntitleDate = DateTime.Parse(Console.ReadLine().Trim());
            //Console.WriteLine("Effective Date? (YYYY-MM-DD)");
            //EffectiveDate = DateTime.Parse(Console.ReadLine().Trim());
            //Console.WriteLine("Expiry Date? (YYYY-MM-DD)");
            //ExpiryDate = DateTime.Parse(Console.ReadLine().Trim());
            try
            {
                var setting = new DAL.TblSetting();
                setting.Query.es.Top = 1;
                setting.Query.SelectAll();
                if (!setting.Query.Load())
                {
                    return false;
                }
                RebateID = int.Parse(setting.RebateID.Value.ToString());
                EntitleDate = setting.EntitleDate.Value;
                EffectiveDate = setting.EffectiveDate.Value;
                ExpiryDate = setting.ExpiryDate.Value;
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
